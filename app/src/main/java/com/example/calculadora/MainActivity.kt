package com.example.calculadora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar : Button
    private lateinit var btnSalir : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar.setOnClickListener { ingresar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes(){
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)
        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContraseña)
    }

    private fun ingresar(){
        var strUsuario : String
        var strContraseña : String

        strUsuario = applicationContext.resources.getString(R.string.usuario)
        strContraseña = applicationContext.resources.getString(R.string.contraseña)
        if(strUsuario.toString().equals(txtUsuario.text.toString()) &&
                strContraseña.toString().equals(txtContraseña.text.toString())){

            //Hacer el paquete para enviar información
            var bundle = Bundle()
            bundle.putString("usuario", txtUsuario.text.toString())

            //Hacer intent para llamar otra actividad
            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtras(bundle)

            //Iniciar la actividad esperando a no respiesta
            startActivity(intent)

        }else{
            Toast.makeText(this.applicationContext, "El Usuario o Contraseña no es válido", Toast.LENGTH_SHORT).show()
        }
    }

    private fun salir(){
        var conf = AlertDialog.Builder(this)
        conf.setTitle("Calculadora")
        conf.setMessage("¿Salir de la App?")
        conf.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        conf.setNegativeButton("Cancelar"){dialogInterface,which->}
        conf.show()
    }
}